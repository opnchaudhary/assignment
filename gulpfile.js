var gulp = require('gulp');
var uglify = require('gulp-uglify');
var cleanCSS = require("gulp-clean-css");

gulp.task('scripts', function(){
	console.log("working on scripts");
	gulp.src('src/*.js').pipe(uglify()).pipe(gulp.dest('assets'))
});

gulp.task('styles', function(){
	return gul.src('src/*.css')
		.pipe(cleanCSS({debug:true}, function(details) {
			console.log(details.name + ': ' + details.stats.originalSize);
			console.log(details.name+': '+ details.stats.minifiedSize);
	}))
	.pipe(gulp.dest('assets'));
});

gulp.task('watch', function(){
	gulp.watch('src/*.js', ['scripts']);
	gulp.watch('src/*.css', ['styles']);
});
gulp.task('default',['scripts','styles','watch']);
