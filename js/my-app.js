// Initialize your app
var myApp = new Framework7();

// Export selectors engine
var $$ = Dom7;

// Add views
var view1 = myApp.addView('#view-1');


/*========query form=======*/
$$('.form-to-data').on('click', function(){

	var formData = myApp.formToData('#my-form');

  // Accessing an individual variable
  console.log(formData.name);
  
  alert(JSON.stringify(formData));

  myApp.popup('.popup-about');

}); 

console.log('running js code...');


(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


window.fbAsyncInit = function(){
	FB.init({
		appId : '493602991011463',
		cookie: true,
		xfbml : true,
		version: 'v2.8'
	});
	$(document).trigger('fbInit');
}


function statusChangeCallback(response){
	console.log('statusChangeCallback');
	console.log(response);
	if(response.status === 'connected'){
		testAPI();
	} else {
		$('#fb_status').html("");
		$('#fb_login').show();
		$('#secure_area').hide();
	}
}
function checkLoginState() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
}

function testAPI() {
	console.log('Welcome! fetching your information.... ');
	FB.api('/me', function(response) {
		console.log('Successful login for : '+ response.name);
		$('#fb_status').html('Welcome '+response.name+'!');
		$('#fb_login').hide();
		$('#secure_area').show();
		loadNews('general');
	});
}
function logoutPerson(){
	FB.logout(function(response){
		//Person is now logged out
		$('#fb_status').html("");
		$('#fb_login').show();
		$('#secure_area').hide();
	});
}
function loadNews(category){
	console.log("Load news from "+category+" category");
	$('#newsfeed').html("");
	$$.get('https://newsapi.org/v2/top-headlines?apiKey=f9479eb137314417a1769b7fb8b0af67&category='+category, 
		function(data){
			var jsonData = JSON.parse(data);
			var articles = jsonData.articles;
			articles.forEach(function(article){
				article_html = '<li class="item-content">'+
				'<div class="item-content">'+
				'<div class="item-media">'+
				'<img src="'+article.urlToImage+'">'+
				'</div>'+
				'<div class="item-inner">'+
				' <div class="item-title-row">'+
				'  <div class="item-title">'+article.title+'</div>'+
				' <div class="item-after">'+article.url+'</div>'+
				'</div>'+
				'<div class="item-subtitle">Author: '+article.author+'</div>'+
				'<div class="item-text">Additional description text</div>'+
				' </div>'+
				'</div>'+
				'</li>';
				$('#newsfeed').append(article_html);
			});
		});
}
$(document).bind('fbInit', function() {
	console.log('FB SDK loaded');
	checkLoginState();
});
